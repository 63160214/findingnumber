import java.util.HashMap;
import java.util.Scanner;

public class finding {
    static int findingNumber(int[] num, int n) {
        HashMap<Integer, Integer> h = new HashMap<>();
        long sum = 0, sum1 = 0;
        for (int i = 0; i < n; i++) {
            if (!h.containsKey(num[i])) {
                sum += num[i];
                h.put(num[i], 1);
            }
            sum1 += num[i];
        }  
        return (int) (2 * (sum) - sum1);
    }

    public static void main(String args[]) {
        Scanner kb = new Scanner(System.in);
        System.out.println("Enter the numbers");
        int[] x = new int[9];
        for (int i = 0; i < 9; i++) {
            x[i] = kb.nextInt();
            
        }
        System.out.println(findingNumber(x, x.length));
    }
}